/system script 
add name="cert.pl-hole-for-mikrotik-dns-download" source={/tool fetch url="https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/raw/main/cert.pl-hole-for-mikrotik-dns.rsc" mode=https}
add name="cert.pl-hole-for-mikrotik-dns-autoupdate" source={/ip dns static
:foreach i in=[find comment=cert.pl-hole-for-mikrotik-dns] do={
  :do {
      remove $i;
  } on-error={ :put "cannot be deleted"};
 };
/import file-name=cert.pl-hole-for-mikrotik-dns.rsc;
/file remove cert.pl-hole-for-mikrotik-dns.rsc}
/system scheduler 
add interval=24h name="cert.pl-hole-for-mikrotik-dns-download" start-time=02:05:00 on-event=cert.pl-hole-for-mikrotik-dns-download
add interval=24h name="cert.pl-hole-for-mikrotik-dns-autoupdate" start-time=02:10:00 on-event=cert.pl-hole-for-mikrotik-dns-autoupdate
