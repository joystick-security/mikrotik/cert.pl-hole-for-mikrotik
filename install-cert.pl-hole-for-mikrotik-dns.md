## Install cert.pl-hole-for-mikrotik-dns
download install-dns.rsc
```
/tool fetch url="https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/raw/main/install-dns.rsc" mode=https
```
run script in terminal
```
/import file-name=install-dns.rsc
```
**Auto update is included in the installation script** -> [install-dns.rsc](https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/raw/main/install-dns.rsc)

**Important**
[cert.pl-hole-for-mikrotik-dns.rsc](https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/raw/main/cert.pl-hole-for-mikrotik-dns.rsc) is is generated and uploaded to this repository daily before 2:00 AM (**UTC+0**).

| Scheduler |
| ------ |
cert.pl-hole-for-mikrotik-download starts at 02:05 AM (router time).
cert.pl-hole-for-mikrotik-autoupdate starts at 02:10 AM (router time).
