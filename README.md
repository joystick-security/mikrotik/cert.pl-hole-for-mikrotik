# CERT.pl hole for mikrotik

> https://cert.pl/en/posts/2020/03/malicious_domains/

Original .rsc list is truncated to 4096 bytes. I decided to generate a complete list based on https://hole.cert.pl/domains/domains.txt <br /><br />
In the cert.pl-hole-for-mikrotik.rsc file, I especially add FQDN to the firewall address-list. Because MikroTik can transform FQDN into IP addresses dynamically. So if a malicious website changes IP address and FQDN remains unchanged it will protect us again. It's also a plus if the malicious site has multiple A records. <br />
[Install version with FQDN](https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/blob/main/install-cert.pl-hole-for-mikrotik.md)

The version with static DNS records, which is the most accurate. <br />
For this option to work we need to make sure that **DoH/DoT is disabled in the web browser.** <br />
[Install version with DNS](https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/blob/main/install-cert.pl-hole-for-mikrotik-dns.md)

**I recommend a minimum of 512MB RAM on the router.**
