/system script 
add name="cert.pl-hole-for-mikrotik-download" source={/tool fetch url="https://gitlab.com/joystick-security/mikrotik/cert.pl-hole-for-mikrotik/-/raw/main/cert.pl-hole-for-mikrotik.rsc" mode=https}
add name="cert.pl-hole-for-mikrotik-autoupdate" source={/ip firewall address-list
:foreach i in=[find list=cert.pl-hole-for-mikrotik] do={
  :do {
      remove $i;
  } on-error={ :put "cannot be deleted"};
 };
/import file-name=cert.pl-hole-for-mikrotik.rsc;
/file remove cert.pl-hole-for-mikrotik.rsc}
/system scheduler 
add interval=24h name="cert.pl-hole-for-mikrotik-download" start-time=02:05:00 on-event=cert.pl-hole-for-mikrotik-download
add interval=24h name="cert.pl-hole-for-mikrotik-autoupdate" start-time=02:10:00 on-event=cert.pl-hole-for-mikrotik-autoupdate
/ip firewall filter
add chain=forward dst-address-list="cert.pl-hole-for-mikrotik" action=drop log=yes log-prefix="cert.pl-hole-for-mikrotik"
